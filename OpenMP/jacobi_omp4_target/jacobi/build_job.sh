#!/bin/bash -l 

if [ $# -lt 1 ]
then
    echo "no compiler provided (options: cray, intel, pgi)"
else
    COMPILER=$1
    HOSTNAME=`hostname | cut -d- -f1`

    echo "Compiling Jacobi and Jacobi Accelerated version with $COMPILER on $HOSTNAME"

    if [ $HOSTNAME == "chester" -o $HOSTNAME == "titan" ]
    then
        module unload PrgEnv-pgi
        module unload PrgEnv-cray
        module unload PrgEnv-gnu
        module unload PrgEnv-intel
        module unload craype-accel-nvidia35
        
        make clean
        
        if [ $COMPILER == "intel" ]
        then
            module load PrgEnv-intel
            module swap intel/15.0.2.164
            module list
            make intel
        
            make clean
            module load craype-accel-nvidia35
            module list
            make intel-acc

            make clean
            make intel-acc-host
        elif [ $COMPILER == "cray" ]
        then
            module load PrgEnv-cray
            module swap cce/8.4.6
            module list
            make cray
        
            make clean
            module load craype-accel-nvidia35
            module list
            make cray-acc

            make clean
            module unload craype-accel-nvidia35
            module load craype-accel-host
            module list
            make cray-acc-host
        elif [ $COMPILER == "pgi" ]
        then
            module load PrgEnv-pgi
            module load craype-accel-nvidia35
            module list
            make pgi-openacc
        fi
    elif [[ "$HOSTNAME" == beacon* ]]
    then
        make -f Makefile.beacon clean

        if [ $COMPILER == "intel" ]
        then
            make -f Makefile.beacon intel
            make -f Makefile.beacon clean
            make -f Makefile.beacon intel-acc
            make -f Makefile.beacon clean
            make -f Makefile.beacon intel-acc-host
            make -f Makefile.beacon clean
            make -f Makefile.beacon intel-mic
            make -f Makefile.beacon clean
            make -f Makefile.beacon intel-acc-host-mic
            make -f Makefile.beacon clean
            make -f Makefile.beacon intelT
            make -f Makefile.beacon clean
            make -f Makefile.beacon intelT-host
            make -f Makefile.beacon clean
            make -f Makefile.beacon intelT-host-mic
        elif [ $COMPILER == "cray" -o $COMPILER == "pgi" ]
        then
            echo "$COMPILER not supported on $HOSTNAME"
        fi
    elif [ $HOSTNAME == "eos" ]
    then
        module unload PrgEnv-pgi
        module unload PrgEnv-cray
        module unload PrgEnv-gnu
        module unload PrgEnv-intel

        make -f Makefile.eos clean

        if [ $COMPILER == "intel" ]
        then
            module load PrgEnv-intel
            module swap intel/16.0.1.150
            module list
            make -f Makefile.eos intel

            make -f Makefile.eos clean
            module list
            make -f Makefile.eos intel-acc-host
        elif [ $COMPILER == "cray" ]
        then
            module load PrgEnv-cray
            module swap cce/8.4.5
            module list
            make -f Makefile.eos cray
        elif [ $COMPILER == "pgi" ]
        then
            echo "$COMPILER not supported on $HOSTNAME"
        fi
    elif [[ "${HOSTNAME}" == *"h2o"* ]]
    then   
        module unload PrgEnv-pgi
        module unload PrgEnv-cray
        module unload PrgEnv-gnu
        module unload PrgEnv-intel

        make clean

        if [ $COMPILER == "intel" ]
        then
        echo "intel compiler can not be used to compile this program in this host, plese use cray compiler"
        elif [ $COMPILER == "cray" ]
        then
        echo "using cray compiler"
            module load PrgEnv-cray
            module swap cce cce/8.5.8
            module list
            make cray
            
            make clean
            module load craype-accel-nvidia35
            module list
            make cray-acc

            make clean
            module unload craype-accel-nvidia35
            module load craype-accel-host
            module list
            make cray-acc-host
        elif [ $COMPILER == "pgi" ]
        then
            echo "pgi compiler can not be used to compile this program in this host, plese use cray compiler"
        
        else
            echo "please provide a valid compiler name"
        fi
        
    else
       echo "host not recognized"
    fi
fi
