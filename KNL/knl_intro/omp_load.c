/*
 * =====================================================================================
 *
 *       Filename:  omp_load.c
 *
 *    Description:  Load generator using OpenMP runtime
 *        Created:  6/23/2017      
 *        Version:  1.0
 *         Author:  Richard Todd Evans
 *   Organization:  TACC
 *
 * =====================================================================================
 */

#include<omp.h>
#include<stdio.h>

int main() {

  int i = 0;  
  int N = 300;

#pragma omp parallel
  while(1);

  return 0;
}

