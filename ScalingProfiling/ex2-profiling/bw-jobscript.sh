#!/bin/bash -l
#PBS -l nodes=1:ppn=32:xe
#PBS -l walltime=15:00
#PBS -j oe
#PBS -N ex2

# In this exercise, we will run our TAU-instrumented miniFE job at a couple of
# scales and use TAU to identify which aspects of the code contributed most 
# to poor scaling

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $HOME/scratch/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-s2pi2017/ScalingProfiling}
SCRATCH=$HOME/scratch
. /opt/modules/default/init/bash
module swap PrgEnv-cray PrgEnv-intel
module load tau

cd $PBS_O_WORKDIR

# path to the miniFE executable we built:
ex=$HOME/$training_dir/ex2-profiling/build/miniFE-tau.x

# A 150x150x150 miniFE job should finish within 5 minutes on a single core 
# of a BW node
sz=150
cmd="$ex -nx $sz -ny $sz -nz $sz"

# Each BlueWaters core has 2 hyperthreads, which PBS views as 2 CPUs, but 
# for this exercise we want each MPI task to have its own core. The following
# recipe calculates how many MPI ranks we can run on a given number of nodes:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
cpus_per_node=$(lscpu | awk '/^CPU\(s\):/ {print $NF}')
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_mpi_per_node=$((cpus_per_node/cpus_per_task))
max_mpi_ranks=$((PBS_NUM_NODES * max_mpi_per_node))

# we will still time the overall run, to get a sense of how much overhead 
# profiling added. We'll output the time in seconds to be consistent with ex1
TIMEFORMAT=%R

# a report of communication between each pair of ranks is interesting:
export TAU_COMM_MATRIX=1

# let's look at the profile with 1, 2, and 16 MPI processes:
for nranks in 1 2 16 ; do

  # make the run output easy to identify:
  label=tau_profile-sz${sz}-${PBS_NUM_NODES}n-${nranks}mpi-$PBS_JOBID
  # run in a unique directory under $SCRATCH:
  rundir=$SCRATCH/$training_dir/ex2-profiling/$label
  mkdir -p $rundir
  cd $rundir

  echo "profiling miniFE with $nranks MPI processes on $SLURM_NNODES nodes at `date`"
  time aprun -n $nranks $cmd > stdout 2> stderr   

done
echo "finished miniFE runs at `date`"
