Scaling to Petascale Institute - Scaling and Profiling Workshop
===============================================================

Exercise 7: Finding the sweet spot
----------------------------------

In this final exercise we'll build a hybrid MPI/OpenMP version of miniFE and
run it in various combinations and thread and process counts, then plot the
performance of configrations against the total number of cores used. 


.. _Step 1:

Step 1: Build a hybrid miniFE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We did this already in `exercise 4`_, but that executable had TAU 
instrumentation added. For real measurements it's best to build as you would
for production, so we'll do a clean build without TAU::

  $ module unload tau
  $ cd build
  $ make

On Blue Waters, sometimes the TAU module fails to unload fully, and the 
Makefile will complain - you may need to open a new ``ssh`` session to return 
to  a clean environmet - or simply load the tau module back into your 
environment  and make an instrumented exeecutable

.. _exercise 4: file://../ex4-openmp/README.rst


.. _Step 2:

Step 2: Run in various configurations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``cori-jobscript.sh`` now has 2 levels of loop: it will run miniFE with a few
numbers of MPI ranks for each OMP_NUM_THREADS setting, up to 2 full nodes. Each
run will again write to a ``timings.dat`` file, but this time there will be a 
separate file for each OMP_NUM_THREADS setting.

Submit the job and, when it is done, look for the timings files::

  $ sbatch cori-jobscript.sh    # or qsub bw-jobscript.sh
  Submitted batch job 5496995
  $ 
  ...
  $ ls -lt
  -rw-rw---- 1 sleak staff  238 Jun 24 13:34 timings-8thr_per_rank.dat
  -rw-rw---- 1 sleak staff 2287 Jun 24 13:34 slurm-5496995.out
  -rw-rw---- 1 sleak staff  207 Jun 24 13:27 timings-4thr_per_rank.dat
  -rw-rw---- 1 sleak staff  179 Jun 24 13:23 timings-2thr_per_rank.dat
  -rw-rw---- 1 sleak staff  148 Jun 24 13:22 timings-1thr_per_rank.dat

As an optional extra, you can edit ``cori-jobscript.sh``/``bw-jobscript.sh``, 
select the ``#SBATCH``/``#PBS`` directive for 2-node runs, and re-submit the 
job. It will append the timings files with some timings for higher core counts.

.. _Step 3:

Step 3: Plot the timings
^^^^^^^^^^^^^^^^^^^^^^^^

You can again use whichever charting tool you prefer, but the following 
instructions will use gnuplot to plot the timing vs number of cores used,
for each setting of OMP_NUM_THREADS (so for example, with OMP_NUM_THREADS=2,
an 8 core run has 4 MPI ranks)::

  $ gnuplot
  gnuplot> f1="timings-1thr_per_rank.dat"
  gnuplot> f2="timings-2thr_per_rank.dat"
  gnuplot> f4="timings-4thr_per_rank.dat"
  gnuplot> f8="timings-8thr_per_rank.dat"
  gnuplot> set xlabel "ncores"
  gnuplot> set ylabel "walltime(s)"
  gnuplot> set logscale
  gnuplot> plot f1 u 1:4 w lp t "1 omp per rank" s u, \
  >             f2 u 1:4 w lp t "2 omp per rank" s u, \
  >             f4 u 1:4 w lp t "4 omp per rank" s u, \
  >             f8 u 1:4 w lp t "8 omp per rank" s u


(Notice that we used the ``s u`` option to resolve out-of-order rows in the
``timings.dat`` files, as we did in `exercise 5`_)

.. _exercise 5: file://../ex5-weakscaling/README.rst

In the case of this miniFE, the MPI parallelization is much more complete 
than the OpenMP parallelization (you'll remember some of the scaling limiters 
we found in exercise 4), so you'll probably see that fewer OpenMP threads is
consistently faster than more OpenMP threads. 

In a real application however, this might not be the case - there will be some
trade-off between scaling limiters for MPI, such as communication latency over 
the network, and scaling limiters for OpenMP, such as synchronization.
