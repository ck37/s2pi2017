/*
 * =====================================================================================
 *
 *       Filename:  sharing.c
 *
 *    Description:  Sharing data example
 *        Created:  6/23/2017      
 *        Version:  1.0
 *         Author:  Richard Todd Evans
 *   Organization:  TACC
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <omp.h>


int main() {

  int i,j;
  int reps = 1000;
  int N = 8*10000;

  double start, stop, delta;
  double a;

#pragma omp parallel
  a = 0;

  start = omp_get_wtime();  
#pragma omp parallel private(i,j)
  { 
    for (j = 0; j < reps; j++) {

#pragma omp for schedule(static,1)
      for (i = 0; i < N; i++){
	#pragma omp atomic
	a++;  
      }

    }
  }  
  stop  = omp_get_wtime();
  delta = ((double)(stop - start))/reps;
  printf("run time = %fusec\n", 1.0e6*delta);

  printf("sum = %.1f\n", a);
  
  return 0;
}
