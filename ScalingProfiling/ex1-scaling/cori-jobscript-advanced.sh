#!/bin/bash -l
#SBATCH -N 1
#SBATCH -t 10
#SBATCH -L SCRATCH
#SBATCH -C haswell
#SBATCH -J ex1a
##SBATCH --reservation=STPI_hsw

# This version of the exercise has options for looking at OpenMP scaling,
# and for running on more than 1 node

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $SCRATCH/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-s2pi2017/ScalingProfiling}

ex=$HOME/$training_dir/ex1-scaling/build/miniFE.x
# An optional extension is to build an OpenMP-enabled executable and look at
# the scaling with OpenMP. Uncomment the following two lines to use the OpenMP
# executable
#ex=$HOME/$training_dir/ex1-scaling/build/miniFE-omp.x
#export OMP_NUM_THREADS=1

# A 200x200x200 miniFE job should finish within 5 minutes on a single core 
# of a Cori Haswell node:
sz=200
cmd="$ex -nx $sz -ny $sz -nz $sz"

# A Cori Haswell node has 32 cores, each with 2 hyperthreads. Our Slurm 
# setup considers each hyperthread to be a CPU, but for this exercise we want
# each MPI task (or OpenMP thread) to have its own core. The following recipe 
# calculates how many MPI ranks we can run on a given number of nodes:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
cores_per_node=$((SLURM_CPUS_ON_NODE/hyperthreads_per_core))
# a "cpu" in Slurm is a hyperthread:
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_mpi_per_node=$((SLURM_CPUS_ON_NODE/cpus_per_task))
max_mpi_ranks=$((SLURM_NNODES * max_mpi_per_node))

# make the output of 'time' command easy to plot:
TIMEFORMAT=%R

# .. and prepare a gnuplot data file:
timing_file=$SLURM_SUBMIT_DIR/timings-$SLURM_JOB_ID.dat
echo "#nranks" $'\t' "bm_time" $'\t' "walltime" >> $timing_file

# For expediency, we'll do the whole series of scaling runs in the same job
for i in $(seq 0 10) ; do
  ncores=$((2**i)) # 1, 2, 4, 8, ...

  # to begin with we'll use MPI only, no OpenMP, so we have 1 rank per core:
  nranks=$ncores
  [[ $nranks -gt $max_mpi_ranks ]] && break # max we can scale to in this job

  # an optional extension is to look at OpenMP scaling, in which case we'll
  # use only one node, but set OMP_NUM_THREADS to use 1 thread per core:
  #nranks=1
  #[[ $ncores -gt $cores_per_node ]] && break 
  #export OMP_NUM_THREADS=$ncores
  # CPU affinity is important with OpenMP:
  #export OMP_PROC_BIND=true
  #export OMP_PLACES=cores

  nnodes=1
  # for now, we only use 1 node. As an extension you can run at a higher node 
  # count, but for a 2-node test in a 4-node job, we should limit the srun to 
  # only 2 of the nodes (or the performance characteristics will change).
  # You can uncomment this line to limit the node count used by srun
  #nnodes=$(( (nranks+max_mpi_per_node-1)/max_mpi_per_node))

  # similarly, for a multi-node run, there's no need to repeat the single-node
  # runs we already did. You can uncomment one of these lines to skip over
  # already-completed, lower-scale configurations:
  #[[ $nnodes -eq 1 ]] && continue 
  #[[ $nnodes -lt $SLURM_NNODES ]] && continue

  # make the run output easy to identify:
  label=sz${sz}-${SLURM_NNODES}n-${nranks}mpi-$SLURM_JOB_ID
  # run in a unique directory under $SCRATCH:
  rundir=$SCRATCH/$training_dir/ex1-scaling/$label
  mkdir -p $rundir
  cd $rundir

  echo "starting miniFE with $nranks MPI processes on $SLURM_NNODES nodes at `date`"
  # to run the actual application we need:
  #   srun -N$nnodes -n$nranks -c$cpus_per_task --cpu_bind=cores $cmd > stdout 2> stderr
  # We're interested in the total wallclock time each run takes, so we'll wrap the 
  # whole command in a call to bash's 'time' function, and capture it's output
  # in a variable:
  tm=$( { time srun -N$nnodes -n$nranks -c$cpus_per_task --cpu_bind=cores $cmd > stdout 2> stderr ; } 2>&1 )

  # if the run succeeded, we should have a .yaml file in the $rundir with various
  # metrics about how the run went. We'll extract its report of the total run
  # time, and append a gnuplot data file with the timing info:
  bm_tm=$(awk -F: '/Total Program Time/ { print $2 }' *.yaml)
  echo "  $nranks" $'\t' $bm_tm $'\t' $tm >> $timing_file

done
echo "finished miniFE runs at `date`"
