#!/usr/bin/env python
import subprocess
import re

def parse_pprof(folder, top=10):
    """ read pprof output and return a dict of function: (%time, excl_msec).
        We care about the mean, not the total
    pprof output looks like:
Reading Profile files in profile.*

FUNCTION SUMMARY (total):
---------------------------------------------------------------------------------------
%Time    Exclusive    Inclusive       #Call      #Subrs  Inclusive Name
              msec   total msec                          usec/call
---------------------------------------------------------------------------------------
 54.4       194230       196671        6432        6432      30577 miniFE::matvec_std<MatrixType, VectorType>::operator()
 18.7        31728        67776          32    1.28E+07    2118001 miniFE::perform_element_loop
  7.1        25688        25688       19264           0       1333 miniFE::waxpby

FUNCTION SUMMARY (mean):
---------------------------------------------------------------------------------------
%Time    Exclusive    Inclusive       #Call      #Subrs  Inclusive Name
              msec   total msec                          usec/call
---------------------------------------------------------------------------------------
 54.4         6070         6146         201         201      30577 miniFE::matvec_std<MatrixType, VectorType>::operator()
 18.7          992         2118           1      400000    2118001 miniFE::perform_element_loop
  7.1          803          803         602           0       1333 miniFE::waxpby
    """
    pprof = subprocess.Popen('pprof -s -m -p -n {0:d}'.format(top).split(), cwd=folder, stdout=subprocess.PIPE)
    report, err = pprof.communicate()
    if err:
        import sys
        sys.stderr.write(err)
        return # abort, otherwise will go on to yield results

    fields = ('pctime', 'ex_msec', 'inc_msec', 'ncalls', 'nsubs', 'percall', 'name')
    # state machine approach to getting past header:
    SEEKING_MEAN=0
    SEEKING_HEADER=1
    READING_HEADER=2
    PASSED_HEADER=3
    state = SEEKING_MEAN
    result = {}
    for line in report.splitlines():
        if state == SEEKING_MEAN:
            if line == 'FUNCTION SUMMARY (mean):':
                state = SEEKING_HEADER
            continue
        elif state == SEEKING_HEADER:
            if line.startswith('----'):
                state = READING_HEADER
            continue
        elif state == READING_HEADER:
            if line.startswith('----'):
                state = PASSED_HEADER
            continue
        else: # passed heder, each line is a function
            if line == '':
                break
            # 'name' field might have spaces, so instead of a simple 
            # line.split() we'll pack the trailing parts into a single entry:
            parts = line.split()
            name = ' '.join(parts[len(fields)-1:])
            d = dict(zip(fields, parts[:len(fields)]))
            pctime = float(d['pctime'])
            ex_msec = d['ex_msec']
            result[name] = (pctime, ex_msec)
    return result

import getopt
import sys
if __name__ == '__main__':
    
    usage = sys.argv[0] + " [-t top ] -n nprocs -d directory/ [ -n nprocs -d directory ]...\n"
    usage += "runs pprof in each directory and generates a table comparing the top few\n"
    usage += "(10 by default, set with -t) routines in each. Each directory is associated\n"
    usage += "with a number of processors (-n and -d arguments must alternate)\n"
    
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'ht:n:d:', ['help', 'nranks', 'dir'])
    except getopt.GetoptError:
        print usage
        sys.exit(2)

    nprocs_folder = {}  # nprocs: folder
    top = 10
    next_folder = None
    next_nprocs = None

    for opt in opts:
        if opt[0] in ('-h', '--help'):
            print usage
            sys.exit(0)
        elif opt[0] in ('-t', '--top'):
            top = int(opt[1])
        elif opt[0] in ('-n', '--nprocs'):
            if next_nprocs:
                print "missing dir corresponding to nprocs " + nprocs
                print usage
                sys.exit(2)
            elif next_folder:
                nprocs_folder[int(opt[1])] = next_folder
                next_folder = None
            else:
                next_nprocs = int(opt[1])
        elif opt[0] in ('-d', '--dir'):
            if next_folder:
                print "missing nprocs corresponding to dir " + next_folder
                print usage
                sys.exit(2)
            elif next_nprocs:
                nprocs_folder[next_nprocs] = opt[1]
                next_nprocs = None
            else:
                next_folder = opt[1]
        else:
            print(usage)
            sys.exit(2)

    profiles = {}

    # to sort columns (routines) by how much they matter, we'll keep track
    # of the highest %time each function uses and sort by that:
    fn_maxpc = {}  # function_name: highest %time 
    for nprocs,folder in nprocs_folder.items():
        profiles[folder] = parse_pprof(folder, top)
        for fname,fprof in profiles[folder].items():
            if fname in fn_maxpc:
                if fprof[0] > fn_maxpc[fname]:
                    fn_maxpc[fname] = fprof[0]
            else:
                fn_maxpc[fname] = fprof[0]

    sorted_fnames = [ "nproc" ] # first column label
    sorted_fnames += [ n for n in sorted(fn_maxpc, key=fn_maxpc.get, reverse=True) ]

    # formatting so we can make columns look nice:
    re_name = re.compile('(?:.*::)?(?P<name>\w+)')
    shortnames = [ re_name.search(n).group(1) for n in sorted_fnames ]
    fwidth_s = lambda s: str(max(10,len(s)))
    fmts = ' '.join([ "{:"+fwidth_s(s)+"s}" for s in shortnames ])
    #fmts = ' '.join([ "{:"+str(len(s))+"s}" for s in shortnames ])

    # print a header:
    print fmts.format(*shortnames)
    # print the timings for each profile:
    ex_msec = lambda d,n: d[n][1] if n in d else '0'
    for nprocs,folder in sorted(nprocs_folder.items()):
        row = [ str(nprocs) ] + [ ex_msec(profiles[folder], n) for n in sorted_fnames[1:]]
        print fmts.format(*row)

