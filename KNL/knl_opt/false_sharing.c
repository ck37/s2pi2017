#include <stdio.h>
#include <omp.h>


int main() {

  int i,j;
  int reps = 1000;
  int N = 8*10000;
  double sum = 0;

  double start, stop, delta;
  int nthreads = omp_get_max_threads();
  double a[nthreads] __attribute__ ((aligned(64)));
  printf("nthreads %d\n", nthreads);

#pragma omp parallel
  {
    int tid = omp_get_thread_num();
    a[tid] = 0;
  }

  // Loop 1: Naive approach induces false sharing
  start = omp_get_wtime();  
#pragma omp parallel private(i,j)
  { 
    int tid = omp_get_thread_num();
    for (j = 0; j < reps; j++) {
#pragma omp for schedule(static,1)
      for (i = 0; i < N; i++) {
	a[tid]+=i;  
      }
    }
  }  
  stop  = omp_get_wtime();
  delta = ((double)(stop - start))/reps;
  printf("Naive run time = %fusec\n", 1.0e6*delta);

  sum = 0;
  for (j = 0; j < nthreads; j++) {
    sum += a[j];  
  }
  printf("sum      = %.1f\n", sum);


double count[nthreads] __attribute__ ((aligned(64))); 
#pragma omp parallel
  {
    int tid = omp_get_thread_num();
    a[tid] = 0;
    count[tid] = 0;
  }

  // Loop 2: Temporary variable approach removes false sharing
  start = omp_get_wtime();  
#pragma omp parallel private(i,j,count)
  { 
    int tid = omp_get_thread_num();    
    for (j = 0; j < reps; j++) {
#pragma omp for schedule(static,1)
      for (i = 0; i < N; i++) {
	count[tid]+=i;
      }
    }
    a[tid] = count[tid];  
  }  
  stop  = omp_get_wtime();
  delta = ((double)(stop - start))/reps;
  printf("Tmp Var run time = %fusec\n", 1.0e6*delta);

  sum = 0;
  for (j = 0; j < nthreads; j++) {
    sum += a[j];  
  }
  printf("sum      = %.1f\n", sum);

  
  return 0;
}
