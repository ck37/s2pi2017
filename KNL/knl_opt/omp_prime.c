/*
 * =====================================================================================
 *
 *       Filename:  omp_prime.c
 *
 *    Description:  Prime number testing using OpenMP
 *        Created:  6/23/2017      
 *        Version:  1.0
 *         Author:  Richard Todd Evans
 *   Organization:  TACC
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <sys/time.h>

////////////////////////////////
// Calculate # of Primes
///////////////////////////////

int is_prime(int n) {
  int i, max, remainder;
  
  if (n < 2)
    return 0;
  if (n < 4)
    return 1;

  max = (int)sqrt(n) + 1;

  for (i = 2; i < max; i++) {
    remainder =  n % i;
    if (remainder == 0)
      return 0;    
  }
  return 1;
}

int main()
{
  int n,j;
  int N = 20000000;

  struct timeval start, end;
  float delta;

  gettimeofday(&start, NULL);

  j = 0;
#pragma omp parallel for schedule(static) reduction(+:j)
  for (n = 0; n < N; n++) {
    if (is_prime(n))
      j++;
}

  gettimeofday(&end, NULL);
  delta = ((end.tv_sec-start.tv_sec)*1000000u + end.tv_usec-start.tv_usec)/1.e6;
  
  printf("# primes = %d\n", j);
  printf("run time = %fs\n", delta);
  return 0;
}
