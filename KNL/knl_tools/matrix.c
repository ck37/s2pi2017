/*
 * =====================================================================================
 *
 *       Filename:  matrix.c
 *
 *    Description:  OpenMP Ready Matrix-Matrix Multiplication Example
 *        Created:  6/23/2017      
 *        Version:  1.0
 *         Author:  Richard Todd Evans
 *   Organization:  TACC
 *
 * =====================================================================================
 */


#include <omp.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/time.h>


int main() {
  int N = 700;

  double flops;
  float delta;
  struct timeval start, end;

  double A[N][N];
  double B[N][N];
  double C[N][N];

  for (int i=0; i<N; i++) {
    for (int j=0; j<N; j++) {
      A[i][j] = i+j;
      B[i][j] = i+i*j;
      C[i][j] = 0.0;
    }
  }


  gettimeofday(&start, NULL);
  for(int i=0;i<N;i++) {
    for(int j=0;j<N;j++) {
      double r = 0;
      for(int k=0;k<N;k++) {
        r += A[i][k]*B[k][j];
      }
      C[i][j] = r;
    }
  }
  gettimeofday(&end, NULL);
  flops = 2*N*N*N;
  delta = ((end.tv_sec*1000000 + end.tv_usec) - (start.tv_sec*1000000 + start.tv_usec))/1.e6;

  printf("run time    = %fs\n", delta);
  printf("GLOPS       = %f\n", flops/delta/1.e9);

  double sum = 0;
  for(int i = 0; i < N; i++)
    for(int j = 0; j < N; j++)
      sum += C[i][j];
  printf("sum         = %f\n", sum);

  return 0;
}

